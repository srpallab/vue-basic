app.component('product-details', {
  template:
  /*html*/
`<div class="product-display">
   <div class="product-container">
     <div class="product-image">
       <!-- image -->
       <img :alt="description" :src="url"/>
     </div>
     <div class="product-info">
       <h1>{{ title }}</h1>
	    <p v-if="inventory > 0">In Stock</p>
	    <p v-else>Sold Out</p>
            <p>Shipping: {{ shipping }}</p>
	    <ul>
	      <li v-for="detail in details" >{{ detail }}</li>
	    </ul>
	    <div v-for="(product, index) in products"
		 :key="product.id"
		 @mouseover="changeVarient(index)"
		 class="color-circle"
		 :style="{ backgroundColor:  product.color }">
	    </div>
	    <button class="button"
		    :class="{ disabledButton: !inventory }"
                    :disabled="!inventory"
		    @click="addToCart">
	      Add To Cart
	    </button>
          </div>
        </div>
   <review-list v-if="reviews.length" :reviews="reviews"></review-list>
   <review-from @review-submitted="updateReview"></review-from>
 </div>`,
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  data() {
    return {
      selectedVarient: 0,
      reviews: [],
      products: [
	{
	  id: 1122,
	  name: "Socks",
	  brand: "Shafiqur Rahman",
	  description: "Nice compfy socks.",
	  details: ['50% cotton', '30% wool', '20% polyester'],
	  image: "assets/image/socks_blue.jpg",
	  color: "blue",
	  quantity: 10
	},
	{
	  id: 1123,
	  name: "Socks",
	  brand: "Shafiqur Rahman",
	  description: "Nice compfy socks.",
	  details: ['50% cotton', '30% wool', '20% polyester'],
	  image: "assets/image/socks_green.jpg",
	  color: "green",
	  quantity: 0
	}
      ]
    }
  },
  methods: {
    changeVarient(index){
      this.selectedVarient = index
    },
    addToCart(){
      this.$emit('add-to-cart', this.products[this.selectedVarient].id)
    },
    updateReview(review){
      this.reviews.push(review)
    }
  },
  computed: {
    title(){
      return this.products[this.selectedVarient].brand + " " + this.products[this.selectedVarient].name;
    },
    url(){
      return this.products[this.selectedVarient].image
    },
    details(){
      return this.products[this.selectedVarient].details
    },
    description(){
      return this.products[this.selectedVarient].description
    },
    inventory(){
      return this.products[this.selectedVarient].quantity
    },
    shipping(){
      if(this.premium){
	return "Free"
      }
      return 2.99
    }
  }
  
});
